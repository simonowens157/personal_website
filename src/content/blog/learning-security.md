+++
title = "How can I learn about security?"
date = "2019-05-02T13:50:46+02:00"
tags = ["learn", "security"]
categories = ["learning Security"]
description = "We all come from different backgrounds with different goals and desires.  One desire many people have is to learn security - developer or security practioner.  I'll guide you through my learning experience and some resources you can use"
banner = "img/banners/bookcase.jpeg"
+++
Image by Janko Ferlic
## Introduction
This is my introduction

#### What is my background?
  * Beginner?
  * Intermediate?

#### What is my goal?
  * Low outcome
  * Medium outcome
  * High outcome

#### What if I don't know?
  * about curiosity, goals, and learning

## My experience
  * What I did
  * What I would change

## Keys to success for Everyone
  * Learning/patience/happiness
  * Sec1

## Specific Resources

#### Developers
  * Beginner - 
  * Intermediate - 

#### Security Engineers
  * Beginner - 
  * Intermediate - 
