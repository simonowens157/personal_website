+++
title = "About"
description = "More about the dev - Simon Owens"
keywords = ["me","about","simon"]
+++

### Summary
I am a Security Engineer currently living in Indianapolis with my Fiance Elizabeth.  I help businesses on the side because I really enjoy doing security and the extra income is nice.  You can find my work history, education, projects, and more on LinkedIn, Github, and Gitlab.

### BIO
I am from a small town in Southern Indiana and love to do things.  I used to not enjoy learning or working on CS projects until I become more familiar with security.  Working on security challenges was just like solving puzzles for me - this got me back into learning and reading so I could solve more challenges.  I become more familiar with web applications, which are extremely visually rewarding unlike writing C code, which sparked my enjoyment for programming.  After years of practice, I've come to appreciate solving security/architecture problems to help better protect people.  I enjoy working and doing capture the flag challenges (less than 50 hours a week), and spend the rest of the week with family, playing basketball, watching tv, reading, and watching starcraft 2.  I hope to be helpful, loving, patient, and healthy for as long as I live.