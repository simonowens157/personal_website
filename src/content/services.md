+++
title = "Services"
description = "Services I provide to businesses and individuals"
keywords = ["services","contract","hire","prices"]
+++
## Web Application Security
Have your web application or API tested in a variety of forms - DAST, SAST, and penetration testing.  I will walk you through existing issues and empower developers to prevent them in the future.

## Docker Container Security
Docker offers nice functionality and features, but can be easily misconfigured.  I will analyze OS vulnerabilities, package vulnerabilities, permission misconfiguration, secret storage, network communication, logging, monitoring, and much more.

### Network/Infrastructure Security
Analyze your entire network architecture for flaws or conduct a full penetration test on your business.  Misconfiguration of security gateways, mail servers, and insecure data storage can lead to loss of revenue.

### Infrastructure as Code
Deploy your end-to-end application stack and infrastructure in the cloud with AWS.  I will customize your tech stack to your needs, all as code with tests.  

### Training
Security and infrastructure can be hard - so get support to stay safe and competitive.  I will show you how to quickly deliver software that is secure, tested, and flexible.  All training can be customized to fit your skill level.