Simon Owens Personal Website
==================

This is the website and mobile app for Simon Owen's personal portfolio and blogging. 

Status
==================

* The website is available at https://owens-netsec.com.
* I am making posts when I have free time and have something valuable to share

Running the website
==================

Instead of installing all of the software on your machine I used docker.
[Docker](https://www.docker.com/), you can run a Docker image of this
site that has all the dependencies already setup for you.

1. `git clone` this repo
2. `cd personal_website; ./run-hugo.sh`
3. Go to `http://localhost:8080` to test

Architecture
==================

* [Styled based on](https://www.ybrikman.com/)
* [Gitlab Pages](https://about.gitlab.com/product/pages/): for hosting the website.
* [CloudFlare](https://www.cloudflare.com/): free SSL and CDN.
* [Bootstrap](http://getbootstrap.com/): for CSS, layout, general theme.
* [Font Awesome](http://fortawesome.github.io/Font-Awesome/): for icons.
* [Form Spree](https://formspree.io): for free contact form.
* [Disqus](https://disqus.com): for free comments.
* [Namecheap](https://www.namecheap.com): domain name registration.
* [Flaticon](https://www.flaticon.com/): free icons.
* [Pexels](https://www.pexels.com/): .
* [Hugo](https://gohugo.io/): for web server and site development.
* [Docker](https://www.docker.com/): local test environment.

Using HTML Templates
==================

* You could put your code in a different .html file and load it with .load() http://api.jquery.com/load/
    * $('#targetid').load('somewhere/a.html'); // loads the .html in the #targetid